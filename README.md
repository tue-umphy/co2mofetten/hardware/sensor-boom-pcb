# sensor-boom-pcb

Sensor Boom PCB design

## Increasing the Revision Number

To increase the revision number,
[bumpversion](https://pypi.org/project/bumpversion/) is used. Close KiCAD and
run `bumpversion patch` (or `minor` or `major`) to increase the respective part
of the version.
