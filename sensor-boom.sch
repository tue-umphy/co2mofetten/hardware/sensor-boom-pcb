EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev "v0.5.0"
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Connection ~ 3250 3500
Connection ~ 3250 3600
Connection ~ 3250 3800
Connection ~ 3250 3400
Connection ~ 4150 3900
Connection ~ 3850 3700
Connection ~ 3250 4300
Connection ~ 2900 4300
Connection ~ 2800 4300
Connection ~ 2600 4300
Connection ~ 3950 4100
Connection ~ 3600 3900
Connection ~ 3500 3700
Connection ~ 4000 4300
Connection ~ 4150 3400
Connection ~ 3750 3700
NoConn ~ 4350 2700
NoConn ~ 4350 2800
NoConn ~ 4350 2900
NoConn ~ 5200 4900
NoConn ~ 5200 5000
NoConn ~ 5200 5100
NoConn ~ 4700 4550
NoConn ~ 5750 3600
NoConn ~ 5750 3700
NoConn ~ 5750 3300
NoConn ~ 5750 3200
Wire Wire Line
	2600 4300 2500 4300
Wire Wire Line
	2800 4300 2600 4300
Wire Wire Line
	2800 4300 2900 4300
Wire Wire Line
	2900 4300 3250 4300
Wire Wire Line
	3100 3400 3250 3400
Wire Wire Line
	3100 3500 3250 3500
Wire Wire Line
	3100 3600 3250 3600
Wire Wire Line
	3100 3700 3500 3700
Wire Wire Line
	3100 3800 3250 3800
Wire Wire Line
	3100 3900 3600 3900
Wire Wire Line
	3250 3000 3250 3400
Wire Wire Line
	3250 3400 3250 3500
Wire Wire Line
	3250 3400 3700 3400
Wire Wire Line
	3250 3500 3250 3600
Wire Wire Line
	3250 3500 4200 3500
Wire Wire Line
	3250 3600 3250 3800
Wire Wire Line
	3250 3800 3250 4300
Wire Wire Line
	3250 4300 3250 4600
Wire Wire Line
	3250 4300 4000 4300
Wire Wire Line
	3500 3700 3750 3700
Wire Wire Line
	3500 5000 3500 3700
Wire Wire Line
	3600 3900 4150 3900
Wire Wire Line
	3600 4900 3600 3900
Wire Wire Line
	3700 2400 4350 2400
Wire Wire Line
	3700 3400 3700 2400
Wire Wire Line
	3750 3450 5600 3450
Wire Wire Line
	3750 3700 3750 3450
Wire Wire Line
	3750 3700 3850 3700
Wire Wire Line
	3850 2500 4350 2500
Wire Wire Line
	3850 3700 3850 2500
Wire Wire Line
	3850 3700 4050 3700
Wire Wire Line
	3950 2300 4350 2300
Wire Wire Line
	3950 4100 3950 2300
Wire Wire Line
	4000 4300 4800 4300
Wire Wire Line
	4000 5100 4000 4300
Wire Wire Line
	4050 3700 4050 3800
Wire Wire Line
	4050 3800 4350 3800
Wire Wire Line
	4150 2600 4350 2600
Wire Wire Line
	4150 3400 4150 2600
Wire Wire Line
	4150 3400 5750 3400
Wire Wire Line
	4150 3900 4150 3400
Wire Wire Line
	4150 3900 4350 3900
Wire Wire Line
	4200 3500 4200 3700
Wire Wire Line
	4200 3700 4350 3700
Wire Wire Line
	4400 4900 3600 4900
Wire Wire Line
	4400 5000 3500 5000
Wire Wire Line
	4400 5100 4000 5100
Wire Wire Line
	4800 4300 4800 4550
Wire Wire Line
	4900 4100 4250 4100
Wire Wire Line
	4900 4550 4900 4100
Wire Wire Line
	5600 3450 5600 3500
Wire Wire Line
	5600 3500 5750 3500
Wire Wire Line
	5750 3000 3250 3000
Wire Wire Line
	5750 3100 3350 3100
$Comp
L power:GND #PWR0101
U 1 1 5E6AAEE2
P 3250 4600
F 0 "#PWR0101" H 3250 4350 50  0001 C CNN
F 1 "GND" H 3255 4427 50  0000 C CNN
F 2 "" H 3250 4600 50  0001 C CNN
F 3 "" H 3250 4600 50  0001 C CNN
	1    3250 4600
	1    0    0    -1  
$EndComp
$Comp
L CO2Mofetten:Bosch_BME280_I2C U1
U 1 1 5E189FDC
P 4600 3800
F 0 "U1" H 4828 3896 50  0000 L CNN
F 1 "Bosch_BME280_I2C" H 4828 3805 50  0000 L CNN
F 2 "CO2Mofetten:gy-bme280" H 4600 4250 50  0001 C CNN
F 3 "" H 4600 4150 50  0001 C CNN
	1    4600 3800
	1    0    0    -1  
$EndComp
$Comp
L CO2Mofetten:Sensirion_SCD30 U2
U 1 1 5E2071F0
P 4700 2600
F 0 "U2" H 4978 2651 50  0000 L CNN
F 1 "Sensirion_SCD30" H 4978 2560 50  0000 L CNN
F 2 "CO2Mofetten:Sensirion_SCD30" H 4700 3000 50  0001 C CNN
F 3 "" H 4700 3000 50  0001 C CNN
	1    4700 2600
	1    0    0    -1  
$EndComp
$Comp
L CO2Mofetten:Cubic_CM1106 U3
U 1 1 5E55BA36
P 4800 4950
F 0 "U3" H 4787 4627 50  0000 C CNN
F 1 "Cubic_CM1106" H 4787 4536 50  0000 C CNN
F 2 "CO2Mofetten:Cubic_CM1106" H 4800 5800 50  0001 C CNN
F 3 "" H 4800 5800 50  0001 C CNN
	1    4800 4950
	1    0    0    -1  
$EndComp
$Comp
L CO2Mofetten:RJ45SensorCableConnector J1
U 1 1 5E18862A
P 2700 3800
F 0 "J1" H 2757 4467 50  0000 C CNN
F 1 "RJ45SensorCableConnector" H 2757 4376 50  0000 C CNN
F 2 "CO2Mofetten:RJ45_econ_MEB8.8PG_Horizontal" V 2700 3825 50  0001 C CNN
F 3 "~" V 2700 3825 50  0001 C CNN
	1    2700 3800
	1    0    0    -1  
$EndComp
$Comp
L CO2Mofetten:Theben_CO2 U4
U 1 1 5E6A6357
P 6150 3300
F 0 "U4" H 6478 3295 50  0000 L CNN
F 1 "Theben_CO2" H 6478 3205 50  0000 L CNN
F 2 "CO2Mofetten:Theben_CO2" H 5850 3300 50  0001 C CNN
F 3 "" H 5850 3300 50  0001 C CNN
	1    6150 3300
	1    0    0    -1  
$EndComp
Wire Wire Line
	3100 4100 3350 4100
Wire Wire Line
	4350 3600 4250 3600
Wire Wire Line
	4250 3600 4250 4100
Connection ~ 4250 4100
Wire Wire Line
	4250 4100 3950 4100
NoConn ~ 3100 4000
Wire Wire Line
	3350 3100 3350 4100
Connection ~ 3350 4100
Wire Wire Line
	3350 4100 3950 4100
$EndSCHEMATC
